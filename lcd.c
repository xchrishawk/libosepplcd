/**
   @file	lcd.c

   @brief	Implementation for the libosepplcd library.
   @author	Chris Vig (chris@invictus.so)

   Implementation is based on Hitachi HD44780U data sheet available at:
   https://www.sparkfun.com/datasheets/LCD/HD44780.pdf
*/


/* -- Includes ------------------------------------------------------------- */

#include <stdint.h>

#include <avr/io.h>
#include <util/delay.h>

#include "lcd.h"


/* -- Macros --------------------------------------------------------------- */

/* Sets a bit in a variable or register */
#define SET_BIT(reg, bit) \
  reg |= ( 1 << ( bit ) )

/* Clears a bit in a variable or register */
#define CLR_BIT(reg, bit) \
  reg &= ~ ( 1 << ( bit ) )

/* Conditionally sets or clears a bit in a variable or register */
#define UPDATE_BIT(reg, bit, cond) \
  ( ( cond ) ? ( SET_BIT( reg, bit ) ) : ( CLR_BIT( reg, bit ) ) )

/* Delays with fudge factor */
#define DELAY_FACTOR ( 1.1 )
#define DELAY_US(x) _delay_us( x * DELAY_FACTOR )
#define DELAY_MS(x) _delay_ms( x * DELAY_FACTOR )

/* Register select line */
#define RS_DDR			DDRB
#define RS_PORT			PORTB
#define RS_PIN			PB0

/* Enable line */
#define ENABLE_DDR		DDRB
#define ENABLE_PORT		PORTB
#define ENABLE_PIN		PB1

/* Backlight control line */
#define BACKLIGHT_DDR 		DDRB
#define BACKLIGHT_PORT		PORTB
#define BACKLIGHT_PIN		PB2

/* I/O register */
#define REGISTER_DDR		DDRD
#define REGISTER_PORT		PORTD
#define REGISTER_PIN		PD4
#define REGISTER_MASK		( 0xF0 )


/* -- Variables ------------------------------------------------------------ */

/* State variables for display control */
static bool display_on = false;
static bool cursor_on = false;
static bool blink_on = false;

/* State variables for entry mode select */
static bool cursor_increment = true;
static bool shift_on = false;


/* -- Procedure Prototypes ------------------------------------------------- */

/* -- Commands -- */

/* Executes the initialization sequence */
static void exec_init_sequence( void );

/* Executes the clear display command */
static void exec_clear( void );

/* Executes the go home command */
static void exec_home( void );

/* Executes the entry mode command */
static void exec_entry_mode( void );

/* Executes the display control command */
static void exec_display_control( void );

/* -- I/O -- */

/* Initializes I/O registers
   NOTE - publicly accessible functions are responsible for calling this
   digital I/O prior to calling any other functions in this section */
static void setup_io( bool data_register_select );

/* Output a nibble to the LCD unit through the I/O register */
static void output_nibble( uint8_t byte );

/* Output a byte to the LCD unit through the I/O register */
static void output_byte( uint8_t byte );

/* Sets the nibble in the I/O register */
static void set_register_data( uint8_t byte );

/* Pulses the enable line */
static void pulse_enable( void );

/* -- Utility -- */

/* Returns the low nibble of a byte */
static inline uint8_t low_nibble( uint8_t byte );

/* Returns the high nibble of a byte */
static inline uint8_t high_nibble( uint8_t byte );


/* -- Procedures ----------------------------------------------------------- */

/* Initializes the OSEPP LCD module driver */
void lcd_init( void )
{

  /* NOTE
     This implementation is based on the procedure found at:
     http://web.alfredstate.edu/weimandn/lcd/lcd_initialization/ ..
       lcd_initialization_index.html */

  /* initialize i/o and delay for 100 milliseconds to allow LCD circuitry
     time to power on */
  setup_io( false );
  DELAY_MS( 100 );

  /* initialization sequence and initial function set command */
  exec_init_sequence();

  /* set up display */
  display_on = false;
  cursor_on = false;
  blink_on = false;
  exec_display_control();

  /* clear display */
  exec_clear();

  /* set entry mode */
  cursor_increment = true;
  shift_on = false;
  exec_entry_mode();

} /* lcd_init */


/* Turn backlight on or off */
void lcd_set_backlight( bool on )
{

  /* backlight is controlled by an I/O pin and does not require any register
     I/O (so it can be controlled without calling lcd_init() */
  SET_BIT( BACKLIGHT_DDR, BACKLIGHT_PIN );
  UPDATE_BIT( BACKLIGHT_PORT, BACKLIGHT_PIN, on );

} /* lcd_set_backlight */


/* Turn LCD on or off */
void lcd_set_display( bool on )
{
  setup_io( false );
  display_on = on;
  exec_display_control();
} /* lcd_set_display */


/* Turn underscore cursor on or off */
void lcd_set_cursor( bool on )
{
  setup_io( false );
  cursor_on = on;
  exec_display_control();
} /* lcd_set_cursor */


/* Turn blinking cursor on or off */
void lcd_set_blink( bool on )
{
  setup_io( false );
  blink_on = on;
  exec_display_control();
} /* lcd_set_blink */


/* Clears the LCD display */
void lcd_clear( void )
{
  setup_io( false );
  exec_clear();
} /* lcd_clear */


/* Moves the cursor to the home position */
void lcd_home( void )
{
  setup_io( false );
  exec_home();
} /* lcd_home */


/* Write a character to the LCD */
void lcd_putc( char c )
{
  setup_io( true );
  output_byte( c );
  DELAY_US( 37.0 );
} /* lcd_putc */


/* Writes a string to the LCD */
void lcd_puts( const char* str )
{

  setup_io( true );

  const char* c = str;
  while( * c )
    {
      output_byte( * c );
      DELAY_US( 37.0 );
      c++;
    }

} /* lcd_puts */


/* -- Command Procedures --------------------------------------------------- */

/* Executes the initialization sequence */
static void exec_init_sequence( void )
{

  /* Startup sequence */
  output_nibble( 0x3 );
  DELAY_MS( 4.1 );
  output_nibble( 0x3 );
  DELAY_US( 100.0 );
  output_nibble( 0x3 );
  DELAY_US( 100.0 );

  /* Initial function set command */
  output_byte( 0x28 );
  DELAY_US( 53.0 );

} /* exec_init_sequence */


/* Executes the clear display command */
static void exec_clear( void )
{
  output_byte( 0x01 );
  DELAY_MS( 3.0 );
} /* exec_clear */


/* Executes the go home command */
static void exec_home( void )
{
  output_byte( 0x02 );
  DELAY_MS( 1.52 );
} /* exec_home */


/* Executes the entry mode command */
static void exec_entry_mode( void )
{

  uint8_t command = 0x04;
  UPDATE_BIT( command, 1, cursor_increment );
  UPDATE_BIT( command, 0, shift_on );

  output_byte( command );
  DELAY_US( 37.0 );

} /* exec_entry_mode */


/* Executes the display control command */
static void exec_display_control( void )
{

  uint8_t command = 0x08;
  UPDATE_BIT( command, 2, display_on );
  UPDATE_BIT( command, 1, cursor_on );
  UPDATE_BIT( command, 0, blink_on );

  output_byte( command );
  DELAY_US( 37.0 );

} /* exec_display_control */


/* -- I/O Procedures ------------------------------------------------------- */

/* Initializes I/O registers */
static void setup_io( bool data_register_select )
{

  /* Set I/O direction to OUTPUT for relevant pins */
  SET_BIT( ENABLE_DDR, ENABLE_PIN );
  SET_BIT( RS_DDR, RS_PIN );
  REGISTER_DDR |= REGISTER_MASK;

  /* Set register select pin state as needed */
  UPDATE_BIT( RS_PORT, RS_PIN, data_register_select );

} /* setup_io */


/* Output a nibble to the LCD unit through the I/O register */
static void output_nibble( uint8_t byte )
{
  set_register_data( low_nibble( byte ) );
  pulse_enable();
} /* output_nibble */


/* Output a byte to the LCD unit through the I/O register */
static void output_byte( uint8_t byte )
{
  set_register_data( high_nibble( byte ) );
  pulse_enable();
  set_register_data( low_nibble( byte ) );
  pulse_enable();
} /* output_byte */


/* Sets the nibble in the I/O register */
static void set_register_data( uint8_t byte )
{
  REGISTER_PORT = 
    ( REGISTER_PORT & ~ ( REGISTER_MASK ) ) | ( byte << REGISTER_PIN );
} /* set_register_data */


/* Pulses the enable line */
static void pulse_enable( void )
{
  SET_BIT( ENABLE_PORT, ENABLE_PIN );
  DELAY_US( 0.230 );
  CLR_BIT( ENABLE_PORT, ENABLE_PIN );
} /* pulse_enable */


/* -- Utility Procedures --------------------------------------------------- */

/* Returns the low nibble of a byte */
static inline uint8_t low_nibble( uint8_t byte )
{
  return ( byte & 0x0F );
} /* low_nibble */


/* Returns the high nibble of a byte */
static inline uint8_t high_nibble( uint8_t byte )
{
  return ( byte & 0xF0 ) >> 4;
} /* high_nibble */
