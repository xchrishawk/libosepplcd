/**
   @file	lcd.h

   @brief	Public header for the libosepplcd library.
   @author	Chris Vig (chris@invictus.so)
*/


#ifndef LCD_H
#define LCD_H


/* -- Includes ------------------------------------------------------------- */

#include <stdbool.h>


/* -- Procedure Prototypes ------------------------------------------------- */


/**
   @brief
   Initializes the OSEPP LCD module driver.

   This function must be called once, prior to calling any other OSEPP LCD
   driver functions.
*/
void lcd_init( void );


/**
   @brief
   Turns the backlight of the LCD on or off.

   @param on
   If true, the backlight will be set to on.
*/
void lcd_set_backlight( bool on );


/**
   @brief
   Turns the LCD display of text on or off.

   @param on
   If true, the LCD will be set to on.
*/
void lcd_set_display( bool on );


/**
   @brief
   Turns the underscore cursor on or off.

   @param on
   If true, an underscore will be displayed at the current cursor position.
*/
void lcd_set_cursor( bool on );


/**
   @brief
   Turns the blink cursor on or off.

   @param on
   If true, a blinking rectangle will be displayed at the current cursor 
   position.
*/
void lcd_set_blink( bool on );


/**
   @brief
   Clears all text from the LCD and resets the cursor to the home position.
*/
void lcd_clear( void );


/**
   @brief
   Moves the cursor to the home position (row 0, column 0) on the LCD.
*/
void lcd_home( void );


/**
   @brief
   Writes a character to the LCD at the current cursor position.

   @param c
   The character to write.
*/
void lcd_putc( char c );


/**
   @brief
   Writes a string to the LCD at the current cursor position.

   @param str
   The string to write. Must be a null-terminated ANSI string.
*/
void lcd_puts( const char* str );


/**
   @brief
   Runs a self-test of the OSEPP LCD module driver.
*/
void lcd_test( void );


#endif /* LCD_H */
