/* -- Includes ------------------------------------------------------------- */

#include <util/delay.h>

#include "lcd.h"


/* -- Procedure Prototypes ------------------------------------------------- */

/* Clears the LCD, and then prints a string */
static void print( const char* str );

/* Pauses to allow the user to read text */
static void pause( void );


/* -- Procedures ----------------------------------------------------------- */

/* Runs a self-test of the OSEPP LCD module driver. */
void lcd_test( void )
{

  lcd_init();
  lcd_set_display( true );

  print( "LCD initialized" );
  pause();

  lcd_set_backlight( false );
  print( "Backlight off" );
  pause();

  lcd_set_backlight( true );
  print( "Backlight on" );
  pause();

  lcd_set_display( false );
  print( "Display off" );
  pause();

  lcd_set_display( true );
  print( "Display on" );
  pause();

  lcd_set_cursor( true );
  print( "Cursor on" );
  pause();

  lcd_set_cursor( false );
  print( "Cursor off" );
  pause();

  lcd_set_blink( true );
  print( "Blink on" );
  pause();

  lcd_set_blink( false );
  print( "Blink off" );
  pause();

  print( "Test complete!" );

} /* lcd_test */


/* Clears the LCD, and then prints a string */
static void print( const char* str )
{
  lcd_clear();
  lcd_puts( str );
} /* print */


/* Pauses to allow the user to read text */
static void pause( void )
{
  _delay_ms( 3000.0 );
} /* pause */
