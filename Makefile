# -- Variables ----------------------------------------------------------------

# Program name
LIB_NAME	:= libosepplcd

# Project configuration
MCU		:= atmega328p
F_CPU		:= 16000000
DEBUG		:= -g
OPTIMIZE	:= -O2

# Input files
C_SRCS		:= lcd.c lcd_test.c
S_SRCS		:=

# Targets
LIB_ARCHIVE	:= $(LIB_NAME).a

# Intermediate files
OBJS		:= $(C_SRCS:.c=.o) $(S_SRCS:.S=.o)
DEPS		:= $(C_SRCS:.c=.d)

# Toolchain
AR		:= avr-ar
AS		:= avr-gcc
CC		:= avr-gcc
CPP		:= avr-gcc
LD		:= avr-gcc
OBJCOPY		:= avr-objcopy
OBJDUMP		:= avr-objdump

# Tool flags
ASFLAGS		:= -mmcu=$(MCU)
CFLAGS		:= -mmcu=$(MCU) -Wall $(DEBUG) $(OPTIMIZE)
CPPFLAGS	:= -DF_CPU=$(F_CPU)


# -- Rules --------------------------------------------------------------------

.PHONY : all clean

# Make all primary targets
all : $(LIB_ARCHIVE)

# Archive file
$(LIB_ARCHIVE) : $(OBJS)
	$(AR) rcs $@ $^

# Dependency files
%.d : %.c
	$(CC) $(CPPFLAGS) -MM -MT $(basename $@).o -MT $@ $< > $@

# Cleanup target
clean :
	$(RM) $(LIB_ARCHIVE) $(OBJS) $(DEPS)


# -- Includes -----------------------------------------------------------------

# Include dependency files
-include $(DEPS)
